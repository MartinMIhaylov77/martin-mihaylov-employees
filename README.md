# Martin-Mihaylov-employees

Welcome to Employee Project Analyzer! This app assists you in identifying the pair of employees who have worked together on common projects for the longest period of time.

Just upload your CSV file, and let the magic happen!

## Features

- Cross-Platform Development:  Employee Project Analyzer is all about ensuring a smooth experience for users, no matter what device they're on. I've set it up so that you can write your code once and run it seamlessly across different platforms. And even though I am currently using Blazor and .NET technologies, I've built the app with an eye toward the future. That's why I've moved some components over to a Razor Shared Library. It's like laying the groundwork for potentially adding .NET MAUI down the line. This not only makes our code more reusable and easier to maintain, but it also opens up exciting possibilities for future enhancements.(Check my Hybrid .NET MAUI project:https://gitlab.com/MartinMIhaylov77/hybridrssfeedapp)
- Project Data Analysis: The application enables users to upload CSV files containing project data, which is then analyzed to identify pairs of employees who have collaborated on common projects for the longest duration.
- Scalable Backend: The MartinMihaylovEmployeesAPI encapsulates the backend logic, enhancing the scalability of the services and allowing future migrations or integration with different frontend technologies.


## Implementation Details
- Visual Studio 2022: The app is built using Visual Studio 2022, leveraging its powerful features for .NET development.
- Employee Project Analyzer leverages cutting-edge technologies like .NET 8 and Blazor for a modern development experience.
- It adopts WebAssembly and client-side render mode to optimize performance, ensuring quick and efficient processing without unnecessary round trips to the server.
- Component Sharing: Components and pages are prepared for shared across platforms, demonstrating the flexibility and reusability of components.

![EmployeesSharedComponents](/uploads/2f23ca2509f4fbc36e5560307398a9d3/EmployeesSharedComponents.png)

- Backend API: The MartinMihaylovEmployeesAPI handles backend logic, promoting scalability and ease of maintenance.

![EmployeesAPI](/uploads/9c51a18ad43e2198783f6f413a7d344a/EmployeesAPI.png)

## Feature Enhancements
- Extend Functionality: Additional features can be added to further demonstrate the capabilities of .NET MAUI and Blazor.
- Enhanced Scalability: The backend services are designed to support future scalability, allowing for easy migration to other frontend technologies.



## Deployment

To deploy this project run

- Clone from repository.
- Load by Visual Studio 2022
- Set a multiple Startup projects 
- Select MartinMihaylovEmployeeAPI with MartinMihaylovEmployeeWASM

![GridCompare](/uploads/9da7d4c13ca09030f7b1f0cff1436c91/GridCompare.png)


Thank you for exploring the MartinMihaylovEmployees. Enjoy your experience, and feel free to contribute or extend the app as you see fit! Hints ToDoos: Logger, Unit Testing, Authentication and Authorization

