﻿using CsvHelper.Configuration;
namespace MartinMihaylovEmployees.API.Models
{
    public class EmployeeProjectRawMap : ClassMap<EmployeeProjectRaw>
    {
        public EmployeeProjectRawMap()
        {
            Map(m => m.EmployeeId).Index(0);
            Map(m => m.ProjectId).Index(1);
            Map(m => m.DateFrom).Index(2);
            Map(m => m.DateTo).Index(3);
        }
    }
}
