﻿namespace MartinMihaylovEmployees.API.Models
{
    public class CommonProject
    {
        public int EmployeeId1 { get; set; }

        public int EmployeeId2 { get; set; }

        public int ProjectId { get; set; }

        public int DaysWorked { get; set; }
    }
}
