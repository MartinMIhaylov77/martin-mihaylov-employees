﻿using MartinMihaylovEmployees.API.Services;
using Microsoft.AspNetCore.Mvc;
namespace MartinMihaylovEmployees.API.Controllers
{

    [Route("api/[controller]")]
    [ApiController]
    public class EmployeeProjectController : Controller
    {
        private readonly FileProcessingService _fileProcessingService;

        public EmployeeProjectController(FileProcessingService fileProcessingService)
        {
            _fileProcessingService = fileProcessingService;
        }

        [HttpPost("upload")]
        public async Task<IActionResult> Upload(IFormFile file)
        {
            if (file == null || file.Length == 0)
            {
                return BadRequest("File is empty");
            }

            var result = await _fileProcessingService.ProcessFileAsync(file);
            return Ok(result);
        }
    }
}
