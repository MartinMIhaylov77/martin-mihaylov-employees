﻿using CsvHelper;
using CsvHelper.Configuration;
using CsvHelper.Configuration.Attributes;
using MartinMihaylovEmployees.API.Models;
using NodaTime;
using NodaTime.Text;
using System.ComponentModel;
using System.Globalization;
using System.Threading.Tasks.Dataflow;
namespace MartinMihaylovEmployees.API.Services
{
    public class FileProcessingService
    {
        private readonly List<DateTimeFormatter> _dateFormats;

        public FileProcessingService()
        {
            _dateFormats = new List<DateTimeFormatter>
            {
                new DateTimeFormatter("yyyy-MM-dd", CultureInfo.InvariantCulture),
                new DateTimeFormatter("MM/dd/yyyy", CultureInfo.InvariantCulture),
                new DateTimeFormatter("dd-MM-yyyy", CultureInfo.InvariantCulture)
            };
        }

        public async Task<List<CommonProject>> ProcessFileAsync(IFormFile file)
        {
            var employees = new List<EmployeeProject>();

            using (var reader = new StreamReader(file.OpenReadStream()))
            using (var csv = new CsvReader(reader, new CsvConfiguration(CultureInfo.InvariantCulture)
            {
                Delimiter = ",",
                TrimOptions = TrimOptions.Trim,
                HasHeaderRecord = true
            }))
            {
                csv.Context.RegisterClassMap<EmployeeProjectRawMap>();

                try
                {
                    while (await csv.ReadAsync())
                    {
                        var record = csv.GetRecord<EmployeeProjectRaw>();

                        var dateFrom = ParseDate(record.DateFrom);
                        var dateTo = string.Equals(record.DateTo, "NULL", StringComparison.OrdinalIgnoreCase) ? DateTime.Today : ParseDate(record.DateTo);

                        employees.Add(new EmployeeProject
                        {
                            EmployeeId = record.EmployeeId,
                            ProjectId = record.ProjectId,
                            DateFrom = dateFrom,
                            DateTo = dateTo
                        });
                    }
                }
                catch (Exception)
                {
                    throw;
                }
            }

            return FindCommonProjects(employees);
        }
        private string PreprocessDate(string date)
        {
            var parts = date.Split('/');

            // Ensure day and month parts have leading zeros if necessary
            var day = parts.Length > 0 ? parts[0].PadLeft(2, '0') : "01";
            var month = parts.Length > 1 ? parts[1].PadLeft(2, '0') : "01";
            var year = parts.Length > 2 ? parts[2] : "2000";

            return $"{day}/{month}/{year}";
        }
        private DateTime ParseDate(string date)
        {
            date = PreprocessDate(date);
            foreach (var formatter in _dateFormats)
            {
                var parseResult = formatter.Parse(date);
                if (parseResult.Success)
                {
                    return parseResult.Value.ToDateTimeUnspecified();
                }
            }

            throw new FormatException($"Unable to parse date: {date}");
        }

        private List<CommonProject> FindCommonProjects(List<EmployeeProject> employees)
        {
            var result = new List<CommonProject>();
            var groupedByProject = employees.GroupBy(e => e.ProjectId);
         
            foreach (var group in groupedByProject)
            {
                var projects = group.ToList();

                for (int i = 0; i < projects.Count; i++)
                {
                    for (int j = i + 1; j < projects.Count;  j++)
                    {
                        var emp1 = projects[i];
                        var emp2 = projects[j];

                        var overlapStart = emp1.DateFrom > emp2.DateFrom ? emp1.DateFrom : emp2.DateFrom;
                        var overlapEnd = emp1.DateTo < emp2.DateTo ? emp1.DateTo : emp2.DateTo;

                        if (overlapStart < overlapEnd)
                        {
                            var daysWorked = (overlapEnd - overlapStart).Days;

                            if (emp1.EmployeeId != emp2.EmployeeId)
                            {
                                result.Add(new CommonProject
                                {
                                    EmployeeId1 = emp1.EmployeeId,
                                    EmployeeId2 = emp2.EmployeeId,
                                    ProjectId = group.Key,
                                    DaysWorked = daysWorked
                                });
                            }
                        }
                    }
                }
            }

            return result;
        }
    }

    public class DateTimeFormatter
    {
        private readonly string _format;
        private readonly CultureInfo _culture;

        public DateTimeFormatter(string format, CultureInfo culture)
        {
            _format = format;
            _culture = culture;
        }

        public ParseResult<LocalDate> Parse(string date)
        {
            var pattern = LocalDatePattern.CreateWithInvariantCulture(_format);
            return pattern.Parse(date);
        }
    }
}