using MartinMIhaylovEmployees.WASM;
using MartinMihaylovEmployees.Components.Services;
using Microsoft.AspNetCore.Components.Web;
using Microsoft.AspNetCore.Components.WebAssembly.Hosting;

var builder = WebAssemblyHostBuilder.CreateDefault(args);
builder.RootComponents.Add<App>("#app");
builder.RootComponents.Add<HeadOutlet>("head::after");

builder.Services.AddHttpClient("custom-httpclient", httpClient =>
{
    httpClient.BaseAddress = new Uri("https://localhost:7278/");
});

builder.Services.AddScoped<EmployeeService>();

await builder.Build().RunAsync();
