﻿using MartinMihaylovEmployees.Components.Models;
using System.Net.Http.Json;
namespace MartinMihaylovEmployees.Components.Services
{
    public class EmployeeService
    {
        private readonly IHttpClientFactory _httpClientFactory;

        public EmployeeService(IHttpClientFactory httpClientFactory)
        {
            _httpClientFactory = httpClientFactory;
        }

        public async Task<List<CommonProject>> UploadFileAsync(Stream fileStream, string fileName)
        {
            var content = new MultipartFormDataContent();
            content.Add(new StreamContent(fileStream), "file", fileName);

            var httpClient = _httpClientFactory.CreateClient("custom-httpclient");
            var response = await httpClient.PostAsync("api/EmployeeProject/upload", content);
            response.EnsureSuccessStatusCode();

            return await response.Content.ReadFromJsonAsync<List<CommonProject>>();
        }
    }
}
