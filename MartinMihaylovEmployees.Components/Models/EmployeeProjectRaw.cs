﻿namespace MartinMihaylovEmployees.Components.Models
{
    public class EmployeeProjectRaw
    {
        public int EmployeeId { get; set; }

        public int ProjectId { get; set; }

        public string? DateFrom { get; set; }

        public string? DateTo { get; set; }
    }
}
